# Import the GPIO and time libraries
import RPi.GPIO as GPIO
import time
# enxufarem un led al pin 3
# recorda que el led ha d'anar a un resistor de 200k (almenys) per la part més curta del led i l'altra part al pin 3
# la part més llarga del led anirà a un cable directe a un gnd (per exemple el pin 6
#####Morse code ######
CODE = {' ': ' ',
        "'": '.----.',
        '(': '-.--.-',
        ')': '-.--.-',
        ',': '--..--',
        '-': '-....-',
        '.': '.-.-.-',
        '/': '-..-.',
        '0': '-----',
        '1': '.----',
        '2': '..---',
        '3': '...--',
        '4': '....-',
        '5': '.....',
        '6': '-....',
        '7': '--...',
        '8': '---..',
        '9': '----.',
        ':': '---...',
        ';': '-.-.-.',
        '?': '..--..',
        'A': '.-',
        'B': '-...',
        'C': '-.-.',
        'D': '-..',
        'E': '.',
        'F': '..-.',
        'G': '--.',
        'H': '....',
        'I': '..',
        'J': '.---',
        'K': '-.-',
        'L': '.-..',
        'M': '--',
        'N': '-.',
        'O': '---',
        'P': '.--.',
        'Q': '--.-',
        'R': '.-.',
        'S': '...',
        'T': '-',
        'U': '..-',
        'V': '...-',
        'W': '.--',
        'X': '-..-',
        'Y': '-.--',
        'Z': '--..',
        '_': '..--.-'}

######End of morse code######

# Set the pin designation type.
# In this case, we use BCM- the GPIO number- rather than the pin number itself.
GPIO.setmode (GPIO.BOARD)

# So that you don't need to manage non-descriptive numbers,
# set "LIGHT" to 4 so that our code can easily reference the correct pin.
pinLED=3                # LED CONNECTAT AL PIN 3

# Because GPIO pins can act as either digital inputs or outputs,
# we need to designate which way we want to use a given pin.
# This allows us to use functions in the GPIO library in order to properly send and receive signals.
GPIO.setup(pinLED,GPIO.OUT)

try:
    while True:
                paraula = raw_input('Paraula que vols codificar? ')
                for lletra in paraula:
                        #print lletra
                        for simbolMorse in CODE[lletra.upper()]:
                                if simbolMorse == '-':
                                        GPIO.output(pinLED,1)
                                        time.sleep(0.5)
                                        GPIO.output(pinLED,0)
                                        time.sleep(0.2)

                                elif simbolMorse == '.':
                                        GPIO.output(pinLED,1)
                                        time.sleep(0.2)
                                        GPIO.output(pinLED,0)
                                        time.sleep(0.2)

                                else:
                                        time.sleep(0.5)
                        time.sleep(0.5)
except KeyboardInterrupt:
    GPIO.cleanup()
