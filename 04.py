# 4) fer un altre programa que genere un vector amb paraules ja predefinides,
# les presente per pantalla i calcule la longitud de cada paraula que hi ha dintre del vector.
# A mes a mes, fes que cada cop que es mostre una paraula, hi hagi un retras de 0.5 segons
import time

## introduir dades vector:

llistaAnimals = ['gat', 'gos', 'peix','balena','rinoceront','formiga','drac']



## extraure dades vector
# Amb un vector de cadenes:
llistaAnimals = ['gat', 'gos', 'peix','balena','rinoceront','formiga','drac']


# llistem una paraula si i una no
p=True
for animal in llistaAnimals:
	if (p==True):
		print "L'animal es:",animal,", quantitat de lletres:",len(animal)
		time.sleep(0.5)
		p=False
	else:
		p=True

