# Exercicis per realitzar una codificació en Morse a través d'una raspberry pi, un led i Python


## 1) Crear una carpeta amb el vostre cognom_Nom al /home/pi de la raspberry
```
mkdir cognom_Nom
```

## 2) Realitzar els 4 exercicis demanats al principi en aquesta carpeta. 

Els exercicis tindran aquests noms: 01.py, 02.py, 03.py, 04.py

Podem crear-los i editar-los amb: 

```console
nano 01.py
Per guardar ctrl+o + intro. 
Per sortir ctrl+x
```


Fer aquests exercicis a partir dels exemples que hi han:

    1) declarar variables, demanar el nom i mostrar per pantalla el teu nom i una suma de 2 nombres
    2) fer un altre programa en arxiu diferent amb una sentència condicional if – else
    3) fer un altre programa en arxiu diferent amb un bucle que simule una multiplicació, sense utilitzar l'operador *
    4) fer un altre programa que genere un vector amb paraules ja predefinides, les presente per pantalla i calcule la longitud de cada paraula que hi ha dintre del vector. A més a més, fes que cada cop que es mostre una paraula, hi hagi un retràs de 0.5 segons


## 3) Crear el programa morse01.py amb aquesta plantilla: (no poseu accents ni en els comentaris)

```python
# Import the GPIO and time libraries
import RPi.GPIO as GPIO
import time
# enxufarem un led al pin 3
# recorda que el led ha d'anar a un resistor de 200k (almenys) per la part mes curta del led i l'altra part al pin 3
# la part més llarga del led anira a un cable directe a un gnd (per exemple el pin 6

#####Morse code ######
CODE = {' ': ' ',
        "'": '.----.',
        '(': '-.--.-',
        ')': '-.--.-',
        ',': '--..--',
        '-': '-....-',
        '.': '.-.-.-',
        '/': '-..-.',
        '0': '-----',
        '1': '.----',
        '2': '..---',
        '3': '...--',
        '4': '....-',
        '5': '.....',
        '6': '-....',
        '7': '--...',
        '8': '---..',
        '9': '----.',
        ':': '---...',
        ';': '-.-.-.',
        '?': '..--..',
        'A': '.-',
        'B': '-...',
        'C': '-.-.',
        'D': '-..',
        'E': '.',
        'F': '..-.',
        'G': '--.',
        'H': '....',
        'I': '..',
        'J': '.---',
        'K': '-.-',
        'L': '.-..',
        'M': '--',
        'N': '-.',
        'O': '---',
        'P': '.--.',
        'Q': '--.-',
        'R': '.-.',
        'S': '...',
        'T': '-',
        'U': '..-',
        'V': '...-',
        'W': '.--',
        'X': '-..-',
        'Y': '-.--',
        'Z': '--..',
        '_': '..--.-'}

######End of morse code######

GPIO.setmode (GPIO.BOARD)

pinLED=3                # LED CONNECTAT AL PIN 3

GPIO.setup(pinLED,GPIO.OUT)

## resta de codi
## resta de codi

    GPIO.cleanup()		# aquesta instrucció ha d’estar al final de l’script

###### final plantilla ######

```



## 4) Continuar el programa ```+morse01.py```+. Ara ha de demanar una paraula a codificar i imprimir per pantalla les lletres d’aquesta paraula en majúscula.

Per fer-ho penseu que la variable on heu guardat la paraula és com la vista en l’exercici 4 i tindria aquesta forma:

![04.png](images/04.png)


```paraula= [‘h’,’o’,’l’,’a’]```

Per imprimir lletres a majúscula el que farem serà utilitzar la funció ```upper()```. Per exemple per imprimir una lletra en majúscula farem el següent:

```print lletra.upper()```


### 5) Fer el programa ```+morse02.py```+ a partir de l’anterior (```morse01.py```)

```
cp morse01.py morse02.py
nano morse02.py
```

Aquesta vegada ha d’aparèixer les lletres de la paraula amb un retràs de 0.5 segons i al costat els signes morse indicats en la variable CODE que hem pegat a la plantilla inicial.

![05.png](images/05.png)

Per fer-ho podem imprimir a continuació de cada llegra, extreient les lletres de CODE així: 

```+CODE[lletra.upper()]```




## 6) Fer el programa ```+morse03.py```+ a partir de l’anterior.

En aquest programa:

    • A partir de les lletres de la paraula introduïda, engegarem el led 0.5 segons per cada símbol "-" i l’apagarem 0.2 després.
      
    • A partir de les lletres de la paraula introduïda, engegarem el led 0.2 segons per cada símbol "." i l’apagarem 0.2 després.



Al final de la impressió d’una lletra, el led s’ha d’apagar durant 0.5 segons per distingir les diferents lletres

```python
// US DONE UNA PISTA DE COM FER-HO. 
// MIREU COM ENGEGAR EL LED

for simbol in CODE[lletra.upper()]:
                                if simbol == '-':
							...
							...
					   elif simbolMorse == '.':
							...
							...
					   else:
							time.sleep(0.5) 


```


## ESQUEMA CONNEXIÓ LED


![Morse_bb.png](images/Morse_bb.png)




